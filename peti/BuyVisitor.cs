﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace peti
{
    class BuyVisitor : IVisitor
    {
        private const double DVDTax = 0.18;
        private const double VHSTax = 0.10;
        private const double BOOKTax = 0.10;

        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE) { return 0; }
            return DVDItem.Price * (1 + DVDTax);
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * (1 + DVDTax);
        }
        public double Visit(Book Bookitem)
        {
            return Bookitem.Price * (1 + BOOKTax);
        }
    }
}

