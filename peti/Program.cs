﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace peti
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD dvd = new DVD("Fast and Furius 2", DVDType.MOVIE, 150);
            VHS vhs = new VHS("MR. Been", 130);            
            Book book = new Book("Insurgent", 400);
            DVD dvd2 = new DVD("Windows10.exe", DVDType.SOFTWARE, 700);

            BuyVisitor buyvisitor = new BuyVisitor();
            Console.WriteLine(vhs.ToString());
            Console.WriteLine(dvd.ToString());
            Console.WriteLine(book.ToString());
            Console.WriteLine(dvd2.ToString());
            double all = 0;
            all = vhs.Accept(buyvisitor) + dvd.Accept(buyvisitor) + book.Accept(buyvisitor) + dvd2.Accept(buyvisitor);
            Console.WriteLine("Total price of all items is: " + all);

        }
    }
}
