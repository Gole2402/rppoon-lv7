﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prvi_i_drugi
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            double temp = 0;

            for (int write = 0; write < array.Length; write++)
            {
                for (int sortt = 0; sortt < array.Length - 1; sortt++)
                {
                    if (array[sortt] > array[sortt + 1])
                    {
                        temp = array[sortt + 1];
                        array[sortt + 1] = array[sortt];
                        array[sortt] = temp;
                    }
                }
            }

        }
    }
}
