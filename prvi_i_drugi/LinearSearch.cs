﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prvi_i_drugi
{
    class LinearSearch : ISearch
    {
        private double searched;
        public LinearSearch(double x)
        {
            this.searched = x;
        }
        public bool Search(double[] array)
        {
            int n = array.Length;
            for (int i = 0; i < n; i++)
            {
                if (array[i] == searched)
                    return true;
            }
            return false;
        }

    }
}
