﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prvi_i_drugi
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = {10,12,55,0.21,487,-2.11,77,58,-254,72.44,-999.712 };

            ISearch searchstrategy = new LinearSearch(58);
            NumberSequence sequence = new NumberSequence(array);
            SortStrategy sort = new BubbleSort();

            sequence.SetSortStrategy(sort);
            sequence.Sort();
            Console.WriteLine(sequence);
            sequence.SetSearchStrategy(searchstrategy);
            if (sequence.Search() == true)
            {
                Console.WriteLine("Number is here!");
            }
            else
            {
                Console.WriteLine("Number is not here!");
            }    

        }
    }
}
