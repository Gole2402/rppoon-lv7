﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace treci_i_cetvrti
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider provider = new SystemDataProvider();
            FileLogger logger1 = new FileLogger("1.txt");
            FileLogger logger2 = new FileLogger("2.txt");
            ConsoleLogger log = new ConsoleLogger();
            provider.Attach(log);
            provider.Attach(logger1);
            provider.Attach(logger2);

            for (; ; )
            {

                provider.GetAvailableRAM();
                provider.GetCPULoad();


                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
