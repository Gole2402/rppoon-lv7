﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace treci_i_cetvrti
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        //public float GetCPULoad() // treci zad
        //{
        //    float currentLoad = this.CPULoad;
        //    if (currentLoad != this.previousCPULoad)
        //    {
        //        this.Notify();
        //    }
        //    this.previousCPULoad = currentLoad;
        //    return currentLoad;
        //}
        //public float GetAvailableRAM() // treci zad
        //{
        //    float currentRam = this.AvailableRAM; 
        //    return currentRam;
        //}

        public float GetCPULoad() // cetvrti zad
        {
            float currentLoad = this.CPULoad;
            if (currentLoad > 1.1 * this.previousCPULoad || currentLoad < 0.9 * this.previousCPULoad)
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }


        public float GetAvailableRAM() //cetvrti zad
        {
            float currentRAM = this.AvailableRAM;
            if (currentRAM > this.previousRAMAvailable + 0.1 * this.previousRAMAvailable || currentRAM < this.previousRAMAvailable - 0.1 * this.previousRAMAvailable)
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentRAM;
            return currentRAM;
        }


    }
}
